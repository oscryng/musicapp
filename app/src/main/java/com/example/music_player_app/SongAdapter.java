package com.example.music_player_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongAdapter extends RecyclerView.Adapter<SongViewHolder> {

    // Constructor
    SongAdapter(@NonNull Context context, @NonNull ArrayList<Song> songs, @NonNull MainActivity mainActivity) {
        this.context = context;
        this.songs = songs;
        this.mainActivity = mainActivity;
    }
    // Overridden methods
    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // This method is called whenever I need to create a new ViewHolder
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.song_item, parent, false);
        SongViewHolder viewHolder = new SongViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {
        // This method is called whenever an existing ViewHolder needs to be re-used
        // At this point, we need to repopulate the ViewHolder

        Song song = songs.get(position);
        System.out.println(song.imageResourceBig);
        holder.currentSongImage.setImageResource(song.imageResource);
        holder.currentSongTitle.setText(song.songName);
        holder.currentSongAuthor.setText(song.artistName);
        holder.currentSongDuration.setText(song.songDuration);

        if (position == mainActivity.currentSongIndex) {
            holder.selectedBackgroundView.setVisibility(View.VISIBLE);
        }
        else {
            holder.selectedBackgroundView.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("User tapped on song at index " + position);
                mainActivity.onUserSelectionSongAtPosition(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        // Called whenever the RecyclerView needs to know how many items in total to display
        return songs.size();
    }

    // Properties
    Context context;
    ArrayList<Song> songs;
    MainActivity mainActivity;
}
