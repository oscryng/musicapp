package com.example.music_player_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    void onUserSelectionSongAtPosition(int position) {
        switchSong(currentSongIndex, position);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.musiclibrary_screen);

        populateDataModel();

        connectXMLViews();
        setupRecyclerView();
        displayCurrentSong();
        setupButtonHandlers();

        musicLibrary.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.INVISIBLE);
    }

    void populateDataModel() {
        // Initialize properties of playlist
        playlist.name = "Playlist 1";
        playlist.songs = new ArrayList<Song>();

        Song song = new Song();
        song.songName = "Hey!";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "2:52";
        song.imageResource = R.drawable.hey;
        song.mp3Resource = R.raw.hey;
        song.imageResourceBig = R.drawable.hey_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Jazzy Frenchy";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "1:44";
        song.imageResource = R.drawable.jazzyfrenchy;
        song.mp3Resource = R.raw.jazzyfrenchy;
        song.imageResourceBig = R.drawable.jazzyfrenchy_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Happy Rock";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "1:45";
        song.imageResource = R.drawable.happyrock;
        song.mp3Resource = R.raw.happyrock;
        song.imageResourceBig = R.drawable.happyrock_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Creative Minds";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "2:27";
        song.imageResource = R.drawable.creativeminds;
        song.mp3Resource = R.raw.creativeminds;
        song.imageResourceBig = R.drawable.creativeminds_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Little Idea";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "2:49";
        song.imageResource = R.drawable.littleidea;
        song.mp3Resource = R.raw.littleidea;
        song.imageResourceBig = R.drawable.littleidea_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Ukulele";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "2:26";
        song.imageResource = R.drawable.ukulele;
        song.mp3Resource = R.raw.ukulele;
        song.imageResourceBig = R.drawable.ukulele_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "A New Beginning";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "2:34";
        song.imageResource = R.drawable.anewbeginning;
        song.mp3Resource = R.raw.anewbeginning;
        song.imageResourceBig = R.drawable.anewbeginning_big;

        playlist.songs.add(song);

        song = new Song();
        song.songName = "Memories";
        song.artistName = "Benjamin Tissot";
        song.songDuration = "3:50";
        song.imageResource = R.drawable.memories;
        song.mp3Resource = R.raw.memories;
        song.imageResourceBig = R.drawable.memories_big;

        playlist.songs.add(song);
    }

    void connectXMLViews() {
        songsRecyclerView = findViewById(R.id.songList);
        currentSongImage = findViewById(R.id.currentSongImage);
        currentSongTitle = findViewById(R.id.currentSongTitle);
        currentSongAuthor = findViewById(R.id.currentSongAuthor);
        previousSongButton = findViewById(R.id.previousSongButton);
        pauseButton = findViewById(R.id.pauseButton);
        nextSongButton = findViewById(R.id.nextSongButton);
        backArrow = findViewById(R.id.back_arrow);
        progressBar = findViewById(R.id.progressBar);
        musicLibrary = findViewById(R.id.musicLibrary);
        constraintLayout = findViewById(R.id.constraintLayout);
        dropDownMenuButton = findViewById(R.id.dropDownMenu);
        playButton = findViewById(R.id.playButton);

        currentSongImage2 = findViewById(R.id.currentSongImage2);
        currentSongTitle2 = findViewById(R.id.currentSongTitle2);
        currentSongAuthor2 = findViewById(R.id.currentSongAuthor2);
        previousSongButton2 = findViewById(R.id.previousSongButton2);
        pauseButton2 = findViewById(R.id.pauseButton2);
        nextSongButton2 = findViewById(R.id.nextSongButton2);
        playButton2 = findViewById(R.id.playButton2);
    }

    void setupRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        songsRecyclerView.setLayoutManager(layoutManager);

        // Connect the adapter to the recyclerView
        songAdapter = new SongAdapter(this, playlist.songs, this);
        songsRecyclerView.setAdapter(songAdapter);
    }

    void displayCurrentSong() {
        Song currentSong = playlist.songs.get(currentSongIndex);
        currentSongImage.setImageResource(currentSong.imageResource);
        currentSongTitle.setText(currentSong.songName);
        currentSongAuthor.setText(currentSong.artistName);

        currentSongImage2.setImageResource(currentSong.imageResourceBig);
        currentSongTitle2.setText(currentSong.songName);
        currentSongAuthor2.setText(currentSong.artistName);
    }

    void setupButtonHandlers() {
        previousSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Previous song button clicked");
                if (currentSongIndex - 1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseButton.setVisibility(View.INVISIBLE);
                playButton.setVisibility(View.VISIBLE);
                System.out.println("Pause button clicked");
                if (mediaPlayer == null){
                    playCurrentSong();
                }
                else if (mediaPlayer.isPlaying()) {
                    pauseCurrentSong();
                }
                else {
                    playCurrentSong();
                }
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseButton.setVisibility(View.VISIBLE);
                playButton.setVisibility(View.INVISIBLE);
                System.out.println("Play button clicked");
                if (mediaPlayer == null){
                    playCurrentSong();
                }
                else if (mediaPlayer.isPlaying()) {
                    pauseCurrentSong();
                }
                else {
                    playCurrentSong();
                }
            }
        });

        nextSongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Next song button clicked");
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_musicPlayerScreen();
            }
        });

        dropDownMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                open_musicLibraryScreen();
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    if (mediaPlayer!=null) {
                        progressBarHandler.post(new Runnable() {
                            public void run() {
                                progressBar.setProgress(mediaPlayer.getCurrentPosition());
                                //System.out.println("TEST 2 " + mediaPlayer.getCurrentPosition());
                            }
                        });
                    }
                    try {Thread.sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
                }
            }
        }).start();

        previousSongButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Previous song button clicked");
                if (currentSongIndex - 1 >= 0) {
                    switchSong(currentSongIndex, currentSongIndex - 1);
                }
            }
        });

        pauseButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseButton2.setVisibility(View.INVISIBLE);
                playButton2.setVisibility(View.VISIBLE);
                System.out.println("Pause button clicked");
                if (mediaPlayer == null){
                    playCurrentSong();
                }
                else if (mediaPlayer.isPlaying()) {
                    pauseCurrentSong();
                }
                else {
                    playCurrentSong();
                }
            }
        });

        playButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pauseButton2.setVisibility(View.VISIBLE);
                playButton2.setVisibility(View.INVISIBLE);
                System.out.println("Play button clicked");
                if (mediaPlayer == null){
                    playCurrentSong();
                }
                else if (mediaPlayer.isPlaying()) {
                    pauseCurrentSong();
                }
                else {
                    playCurrentSong();
                }
            }
        });

        nextSongButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Next song button clicked");
                if (currentSongIndex + 1 < playlist.songs.size()) {
                    switchSong(currentSongIndex, currentSongIndex + 1);
                }
            }
        });
    }


    void switchSong(int fromIndex, int toIndex) {
        songAdapter.notifyItemChanged(currentSongIndex);
        // Update currentSongIndex
        currentSongIndex = toIndex;

        // Display song
        displayCurrentSong();

        songAdapter.notifyItemChanged(currentSongIndex);

        songsRecyclerView.scrollToPosition(currentSongIndex);

        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            pauseCurrentSong();
            mediaPlayer = null;
            playCurrentSong();
        }
        else {
            mediaPlayer = null;
        }
    }

    void pauseCurrentSong() {
        // TODO: implement pause song
        System.out.println("Pausing song at index " + currentSongIndex);
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    void playCurrentSong() {
        // TODO: implement play song
        System.out.println("Playing song at index " + currentSongIndex);
        if (mediaPlayer == null) {
            Song currentSong = playlist.songs.get(currentSongIndex);
            mediaPlayer = MediaPlayer.create(MainActivity.this, currentSong.mp3Resource);
            progressBar.setMax(mediaPlayer.getDuration());
            System.out.println("TEST: " + mediaPlayer.getDuration());
        }
        mediaPlayer.start();
    }

    private void open_musicPlayerScreen(){
        musicLibrary.setVisibility(View.INVISIBLE);
        constraintLayout.setVisibility(View.VISIBLE);
        }

     private void open_musicLibraryScreen() {
         musicLibrary.setVisibility(View.VISIBLE);
         constraintLayout.setVisibility(View.INVISIBLE);
     }

    // Properties
    Playlist playlist = new Playlist();
    SongAdapter songAdapter;
    Integer currentSongIndex = 0;

    MediaPlayer mediaPlayer = null;
    // XML Views
    ConstraintLayout musicLibrary;
    RecyclerView songsRecyclerView;
    ImageView currentSongImage;
    TextView currentSongTitle;
    TextView currentSongAuthor;
    ImageButton previousSongButton;
    ImageButton pauseButton;
    ImageButton nextSongButton;
    ImageButton backArrow;
    ProgressBar progressBar;
    Handler progressBarHandler = new Handler();
    ConstraintLayout constraintLayout;
    ImageButton dropDownMenuButton;
    ImageButton playButton;

    ImageView currentSongImage2;
    TextView currentSongTitle2;
    TextView currentSongAuthor2;
    ImageButton previousSongButton2;
    ImageButton pauseButton2;
    ImageButton nextSongButton2;
    ImageButton playButton2;

}