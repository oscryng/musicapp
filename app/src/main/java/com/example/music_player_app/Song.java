package com.example.music_player_app;

public class Song {
    String songName;
    String artistName;
    String songDuration;
    int imageResource;
    int mp3Resource;
    int imageResourceBig;
}
