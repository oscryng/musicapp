package com.example.music_player_app;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SongViewHolder extends RecyclerView.ViewHolder {

    // Constructor
    public SongViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        currentSongImage = itemView.findViewById(R.id.smallSongImage);
        currentSongTitle = itemView.findViewById(R.id.smallSongTitle);
        currentSongAuthor = itemView.findViewById(R.id.smallSongAuthor);
        currentSongDuration = itemView.findViewById(R.id.smallSongDuration);
        selectedBackgroundView = itemView.findViewById(R.id.selected_background_view);
    }

    // Properties
    View itemView;
    ImageView currentSongImage;
    TextView currentSongTitle;
    TextView currentSongAuthor;
    TextView currentSongDuration;
    View selectedBackgroundView;
}